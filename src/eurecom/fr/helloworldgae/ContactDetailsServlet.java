package eurecom.fr.helloworldgae;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.*;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class ContactDetailsServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		if (req.getParameter("id") == null) {
			resp.getWriter().println("ID cannot be empty!");
			return;
		}
		// Get the datastore
		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();
		// Get the entity by key
		Entity contact = null;
		try {
			contact = datastore.get(KeyFactory.stringToKey(req
					.getParameter("id")));
		} catch (EntityNotFoundException e) {
			resp.getWriter().println("Sorry, no contact for the given key");
			return;
		}
		// Let's output the basic HTML headers
		PrintWriter out = resp.getWriter();
		resp.setContentType("text/html");
		try{
		if(req.getParameter("type").equals("delete"))
		{
			try{
			datastore.delete(KeyFactory.stringToKey(req.getParameter("id")));
			}
			catch(IllegalArgumentException e)
			{
				resp.getWriter().println("Impossible to delete contact!");
			}
			resp.getWriter().println("Contact correctly deleted!");
			return;
		}
		}
		catch(NullPointerException e)
		{
			//No type parameter
		}
		
		/** Different response type? */
		String responseType = req.getParameter("respType");
		if (responseType != null) {
			if (responseType.equals("json")) {
				// Set header to JSON output
				resp.setContentType("application/json");
				out.println(getJSON(contact, req, resp));
				return;
			} else if (responseType.equals("xml")) {
				resp.setContentType("application/json");
				out.println(getXML(contact, req, resp));
				return;
			}
		}

		out.println("<html><head><title>Contacts list</title></head><body>");
		// Let's build the table headers
		out.println("<table style=\"border: 1px solid black; width: auto; text-align: center;\">"
				+ "<tr><td rowspan=3><img src=\""
				+ contact.getProperty("pict")
				+ "\" alt=\"Profile picture\" border=1/></td>");
		out.println("<td>Name: </td><td>" + contact.getProperty("name")
				+ "</td></tr>" + "<tr><td>Phone: </td><td>"
				+ contact.getProperty("phone") + "</td></tr>"
				+ "<tr><td>Email: </td><td><a href=\"mailto:"
				+ contact.getProperty("email") + "\">"
				+ contact.getProperty("email") + "</a>" + "</td></tr>");
		out.println("</table><a href=\"save?id=" + req.getParameter("id")
				+ "\">Modify</a></body></html>");

	}

	private String getXML(Entity contact, HttpServletRequest req,
			HttpServletResponse resp) {
		// TODO Auto-generated method stub
		return null;
	}

	private String getJSON(Entity contact, HttpServletRequest req,
			HttpServletResponse resp) {
		// Create a JSON array that will contain all the entities converted in a
		// JSON version

		JSONObject contactJSON = new JSONObject();
		try {
			contactJSON.put("name", contact.getProperty("name"));
			contactJSON.put("phone", contact.getProperty("phone"));
			contactJSON.put("id", KeyFactory.keyToString(contact.getKey()));
			contactJSON.put("email", contact.getProperty("email"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return contactJSON.toString();
	}
}
