package eurecom.fr.helloworldgae;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.*;

public class SaveContactServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		//		resp.setContentType("text/plain");
		//		resp.getWriter().println("Save contact servlet. GET method doesn't do anything.");

		resp.setContentType("text/html");
		// Let's output the basic HTML headers
		PrintWriter out = resp.getWriter();
		out.println("<html><head><title>Modify a contact</title></head><body>");
		// Get the datastore
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		// Get the entity by key
		Entity contact = null;
		String name = "", phone = "", pict = "", email = "", id ="";
		Long longid = new Long(-1);
		try {
			id = req.getParameter("id");
			contact = datastore.get(KeyFactory.stringToKey(id));

			longid = (contact.getProperty("id") != null) ? (Long) contact.getProperty("id") : new Long(-1);
			name = (contact.getProperty("name") != null) ? (String) contact.getProperty("name") : "";
			phone = (contact.getProperty("phone") != null) ? (String) contact.getProperty("phone") : "";
			email = (contact.getProperty("email") != null) ? (String) contact.getProperty("email") : "";
			pict = (contact.getProperty("pict") != null) ? (String) contact.getProperty("pict") : "";
		} 
		catch (EntityNotFoundException e) {
			resp.getWriter().println("<p>Creating a new contact</p>");
		} 
		catch (NullPointerException e) {
			// id parameter not present in the URL
			resp.getWriter().println("<p>Creating a new contact</p>");
		}
		out.println("<form action=\"save\" method=\"post\" name=\"contact\">");
		// Let's build the form
		out.println("<label>Name: </label><input name=\"name\" value=\"" + name + "\"/><br/>"
				+ "<label>Phone: </label><input name=\"phone\" value=\"" + phone + "\"/><br/>"
				+ "<label>Email: </label><input name=\"email\" value=\"" + email + "\"/><br/>"
				+ "<label>Picture: </label><input name=\"pict\" value=\"" + pict + "\"/><br/>"
				+ "<input type=\"hidden\" name=\"contactId\" value=\"" + longid + "\"/><br/>"
				+ "<input type=\"hidden\" name=\"id\" value=\"" + id + "\"/>");
		out.println("<br/><input type=\"submit\" value=\"Continue\"/></form></body></html>");
	}
	/**
	 * Save a contact in the DB. The contact can be new or already existent.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// Retrieve informations from the request
		String contactName = null;
		String contactId = null;
		String id = null;
		String contactPhone = null;
		String contactEmail = null;
		String contactPict = null;

		try{
			id = req.getParameter("id");
			contactName = req.getParameter("name");
			contactId = req.getParameter("contactId");
			contactPhone = req.getParameter("phone");
			contactEmail = req.getParameter("email");
			contactPict = req.getParameter("pict");
		}
		catch(Exception e)
		{
			System.out.println("Error in retrieve paramenter " + e.getMessage() + e.getStackTrace());
		}

		if(contactName == null || contactPhone == null || contactEmail == null || contactPict == null )
		{
			throw new IOException("Error in getting parameters");
		}
		// Take a reference of the datastore
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		// Generate or retrieve the key associated with an existent contact
		// Create or modify the entity associated with the contact
		Query query = new Query("Contact").addSort("id",
				Query.SortDirection.DESCENDING);
		List<Entity> contacts = datastore.prepare(query).asList(
				FetchOptions.Builder.withDefaults());
		Long i = new Long(-1);
		if(!contacts.isEmpty())
			i = (Long)contacts.get(0).getProperty("id")+1;
		if(!contactId.equals("-1")){
			i = Long.parseLong(contactId);
		}
		
		if(i==-1)
			throw new IOException("Error in id of the contact!");
		
		Entity contact;
		contact = new Entity("Contact", i);
		contact.setProperty("id", i);
		contact.setProperty("name", contactName);
		contact.setProperty("phone", contactPhone);
		contact.setProperty("email", contactEmail);
		contact.setProperty("pict", contactPict);
		// Save in the Datastore
		datastore.put(contact);
		resp.getWriter().println("Contact " + contactName + " saved with key " +
				KeyFactory.keyToString(contact.getKey()) + "!");
	}

}
