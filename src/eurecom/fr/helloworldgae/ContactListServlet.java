package eurecom.fr.helloworldgae;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.*;
import com.google.appengine.labs.repackaged.org.json.*;

import java.io.*;

import org.w3c.dom.Element;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import org.w3c.dom.Document;

public class ContactListServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();
		// Take the list of contacts ordered by name
		Query query = new Query("Contact").addSort("name",
				Query.SortDirection.ASCENDING);
		List<Entity> contacts = datastore.prepare(query).asList(
				FetchOptions.Builder.withDefaults());
		// Let's output the basic HTML headers
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();

		/** Different response type? */
		String responseType = req.getParameter("respType");
		if (responseType != null) {
			if (responseType.equals("json")) {
				// Set header to JSON output
				resp.setContentType("application/json");
				out.println(getJSON(contacts, req, resp));
				return;
			} else if (responseType.equals("xml")) {
				resp.setContentType("application/json");
				out.println(getXML(contacts, req, resp));
				return;
			}
		}

		out.println("<html><head><title>Contacts list</title></head><body>");
		if (contacts.isEmpty()) {
			out.println("<h1>Your list is empty!</h1>");
		} else {
			// Let's build the table headers
			out.println("<table style=\"border: 1px solid black; width: 100%; text-align: center;\">"
					+ "<tr><th>Name</th><th>Phone Number</th><th>Details</th><th>Delete</th></tr>");
			for (Entity contact : contacts) {
				out.println("<tr><td>" + contact.getProperty("name") + "</td>"
						+ "<td>" + contact.getProperty("phone") + "</td>"
						+ "<td><a href=\"contactdetails?id="
						+ KeyFactory.keyToString(contact.getKey())
						+ "\">details</a></td>" 
						+ "<td><a href=\"contactdetails?id="
						+ KeyFactory.keyToString(contact.getKey())
						+ "&type=delete\">delete</a></td>"
						+ "</tr>");
			}
			out.println("</table>");
		}
		out.println("</body></html>");
	}

	private String getXML(List<Entity> contacts, HttpServletRequest req,
			HttpServletResponse resp) {
//		Integer i = 0;
//		String xmlString = null;
//		try{
//		//Creating an empty XML Document
//        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
//        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
//        Document doc = docBuilder.newDocument();
//        
//        Element root = doc.createElement("contactList");
//        doc.appendChild(root);
//        
//        for(Entity e : contacts)
//        {
//        	Element contact = doc.createElement(i.toString());
//        	i++;
//        	root.appendChild(contact);
//        	Element name = doc.createElement((String)e.getProperty("name"));
//        	contact.appendChild(name);
//        	Element phone = doc.createElement((String)e.getProperty("phone"));
//        	contact.appendChild(phone);
//        	Element email = doc.createElement((String)e.getProperty("email"));
//        	contact.appendChild(email);
//        	Element id = doc.createElement(KeyFactory.keyToString(e.getKey()));
//        	contact.appendChild(id);
//        	
//        }
//      //Output the XML to a string
//
//        //set up a transformer
//        TransformerFactory transfac = TransformerFactory.newInstance();
//        Transformer trans = transfac.newTransformer();
//        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
//        trans.setOutputProperty(OutputKeys.INDENT, "yes");
//
//        //create string from xml tree
//        StringWriter sw = new StringWriter();
//        StreamResult result = new StreamResult(sw);
//        DOMSource source = new DOMSource(doc);
//        trans.transform(source, result);
//        xmlString = sw.toString();
//		}
//		catch (Exception e)
//		{
//			
//		}
//        
//		return xmlString;
		return null;
	}

	private String getJSON(List<Entity> contacts, HttpServletRequest req,
			HttpServletResponse resp) {
		// Create a JSON array that will contain all the entities converted in a
		// JSON version
		JSONArray results = new JSONArray();
		for (Entity contact : contacts) {
			JSONObject contactJSON = new JSONObject();
			try {
				contactJSON.put("name", contact.getProperty("name"));
				contactJSON.put("phone", contact.getProperty("phone"));
				contactJSON.put("id", KeyFactory.keyToString(contact.getKey()));
				//contactJSON.put("userid", contact.getProperty("id"));
				contactJSON.put("email", contact.getProperty("email"));
				contactJSON.put("pict", contact.getProperty("pict"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			results.put(contactJSON);
		}
		return results.toString();
	}
}
